<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<form action="#">
<table>
	<tr>
		<td>${testel.question}</td>
	</tr>
	<tr>
		<td>
		<P>
		<hr>
		<c:forEach items="${testel.variants}" var="varnt">

			<input type="radio" name="sample" checked=1 value="${varnt.id}">${varnt.answer}<br>

		</c:forEach>
		<hr>
		</P>
		</td>
	</tr>
	<tr>
		<td colspan="2"><input id="btn" type="submit" value="Дальше"></td>
	</tr>
</table>
</form>

<script type="text/javascript">
	$(document).ready(function() {
		$("#btn").click(function() {
			var chng = $("form :radio:checked").val();
			$("#content_pane").load("getNext.htm?answId=" + chng);
			return false;
		});
	});
</script>
