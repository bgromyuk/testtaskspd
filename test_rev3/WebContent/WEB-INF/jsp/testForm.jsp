<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<style type="text/css">
.even {
	background-color: silver;
}
</style>

<title>Registration Page</title>

<script type='text/javascript'
	src='http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js'></script>
<script type='text/javascript' src='../js/jquery.timers.js'></script>

<script type="text/javascript">
	
</script>
</head>
<body>

<div id="content_pane"></div>

<div id="counter"></div>



<script type="text/javascript">
	$(document).ready(function() {
		$("#content_pane").load("getNext.htm?answId=-1");
		$("#counter").html("");
		$("#content_pane").everyTime(1000, "timer", function(i) {
			$("#counter").html(i);
			if (i > 30) {
				$("#content_pane").stopTime("timer");
				$("#content_pane").load("getNext.htm?answId=-2");
			}
		});
	});
</script>
</body>
</html>