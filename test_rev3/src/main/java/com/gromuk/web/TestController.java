package com.gromuk.web;


import com.gromuk.app.AppService;
import com.gromuk.persistence.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


import java.util.Map;

@Controller
@RequestMapping("test")
public class TestController implements BaseConstants {

	@Autowired
	private AppService appService;

	public AppService getAppService() {
		return appService;
	}

	public void setAppService(AppService appService) {
		this.appService = appService;
	}

	@RequestMapping("list.htm")
	public String list() throws Exception {
		appService.initAttributes();
		System.out.println("Loading main page");
		return "testForm";
	}

	@RequestMapping("getNext.htm")
	public ModelAndView getNext(@RequestParam(value = "answId", required = true) Long answId) throws Exception {
		TestAttributes testAttributes = appService.getTestAttributes();

		Long nextId = testAttributes.getNextId();
		Long testsCount = testAttributes.getTestsCount();

		System.out.println("Right answers count is " + testAttributes.getCnt());
		Test nextTest=appService.answCheck(answId);


		ModelAndView mav = new ModelAndView();
		if (nextId > testsCount || answId == RESULT_SHOW_ID) {
			mav.addObject("cnt", testAttributes.getCnt());
			mav.addObject("testCnt", testsCount);
			mav.setViewName("result");
		} else {
			testAttributes.setNextId(++nextId);
			mav.addObject("testel",nextTest);
			mav.setViewName("test");
		}

		return mav;
	}

}
