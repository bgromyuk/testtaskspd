package com.gromuk.web;

import org.eclipse.jetty.server.session.JDBCSessionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.DefaultSessionAttributeStore;

import java.util.*;

/**
 * Created by b.gromyuk on 08.01.2015.
 */
@Component
@Scope(value = "session",proxyMode = ScopedProxyMode.TARGET_CLASS)
public class TestAttributes implements BaseConstants{
    private Long nextId = START_TEST_ID;
    private Long cnt = 0l;
    private Long testsCount;
    private static final Long QUESTIONS_CNT = 5l;
    private List<Integer> questIds;
    private List<Long> answerIds;
    private Long testsCountDb;
    private List<Long> rundQusetIds;

    public Long getNextId() {
        return nextId;
    }

    public void setNextId(Long nextId) {
        this.nextId = nextId;
    }

    public Long getCnt() {
        return cnt;
    }

    public void setCnt(Long cnt) {
        this.cnt = cnt;
    }

    public Long getTestsCount() {
        return testsCount;
    }

    public void init(Integer testsCountDb) {
        nextId = START_TEST_ID;
        cnt = 0l;
        this.testsCount = QUESTIONS_CNT;
        questIds = new ArrayList<Integer>();
        answerIds = new ArrayList<Long>();
        this.testsCountDb = testsCountDb.longValue();

        rundQusetIds = new ArrayList<Long>();
        for (long i = 1; i <= testsCountDb; i++) {
            rundQusetIds.add(i);
        }
        Collections.shuffle(rundQusetIds);
    }

    private boolean isTestPass(Integer testId) {
        return questIds.contains(testId);
    }

    private void addTestIdToPassLst(Integer testId) {
        if (!isTestPass(testId)) {
            questIds.add(testId);
        }
    }

    public Boolean isAnswerCorrect(Long answId) {

        return answerIds.contains(answId);
    }

    public void addAnswerIdToLst(Long answId) {
        answerIds.add(answId);
    }


    public Long getRandomTestId() {
        return rundQusetIds.get(nextId.intValue());
    }
}
