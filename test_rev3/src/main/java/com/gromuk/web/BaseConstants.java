package com.gromuk.web;

/**
 * Created by b.gromyuk on 08.01.2015.
 */
public interface BaseConstants {
    final static long RESULT_SHOW_ID = -2;
    final static long START_TEST_ID = 1;
    final static int NO_ID = -1;
}
