package com.gromuk.dao;

import com.gromuk.persistence.Test;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TestDao {
	@Autowired
	HibernateDao baseDao;

	public HibernateDao getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(HibernateDao baseDao) {
		this.baseDao = baseDao;
	}

	public Test findTest(Long id) {
		Criteria criteria = baseDao.createCriteriaForClass(Test.class);
		criteria.add(Restrictions.eq("id", id.intValue()));
		return (Test) baseDao.findByCriteria(criteria);
	}

	public int getTestsCount() {
		Criteria criteria = baseDao.createCriteriaForClass(Test.class);
		criteria.setProjection(Projections.rowCount());

		Long testCnt = (Long) criteria.uniqueResult();
		return testCnt.intValue();
	}

}
