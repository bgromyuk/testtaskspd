package com.gromuk.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.ReplicationMode;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * Superclass for all DAO classes. Contains general methods which should be
 * available in each DAO class.
 */
public class HibernateDao extends HibernateDaoSupport {

	public <T> T saveEntity(final T entity) throws HibernateException {
		getSession().saveOrUpdate(entity);
		return entity;
	}

	public <T> T lockForUpdate(T object) throws HibernateException {
		getSession().lock(object, LockMode.NONE);
		return object;
	}

	public <T> T replicate(T object) throws HibernateException {
		getSession().replicate(object, ReplicationMode.LATEST_VERSION);
		return object;
	}

	public <T> T load(T object, Serializable id) throws HibernateException {
		getSession().load(object, id);
		return object;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> loadList(Class<T> clazz) throws HibernateException {
		return getSession().createCriteria(clazz).list();
	}

	public void deleteEntity(Object entity) throws HibernateException {
		getHibernateTemplate().delete(entity);
	}

	public void delete(Object entity) throws HibernateException {
		getSession().delete(entity);
	}

	public void updateEntity(Object entity) throws HibernateException {
		getHibernateTemplate().update(entity);
	}

	public void evict(Object entity) throws HibernateException {
		getSession().evict(entity);
	}

	public void clearSession() {
		getSession().flush();
		getSession().clear();
	}

	public void saveOrUpdateEntity(Object entity) throws HibernateException {
		getHibernateTemplate().saveOrUpdate(entity);
	}

	public <T> Object get(Class<T> clazz, int id) throws HibernateException {
		return getHibernateTemplate().get(clazz, id);
	}

	@SuppressWarnings("unchecked")
	public Criteria createCriteriaForClass(Class clazz) {
		return getSession().createCriteria(clazz);
	}

	public Object findByCriteria(Criteria criteria) throws HibernateException {
		List<?> list = criteria.list();
		if (!list.isEmpty()) {
			return (Object) list.get(0);
		} else {
			return null;
		}
	}
}