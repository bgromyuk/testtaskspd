package com.gromuk.app;


import com.gromuk.dao.TestDao;
import com.gromuk.persistence.Test;
import com.gromuk.web.BaseConstants;
import com.gromuk.web.TestAttributes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.Map;
import java.util.Random;

@Service
public class AppService implements BaseConstants {
	@Autowired
	private TestDao testDao;

	@Autowired
	private TestAttributes testAttributes;

	public void initAttributes() {
		testAttributes.init(getTestsCount());
	}

	public TestAttributes getTestAttributes() {
		return testAttributes;
	}

	public void setTestAttributes(TestAttributes testAttributes) {
		this.testAttributes = testAttributes;
	}

	public TestDao getTestDao() {
		return testDao;
	}

	public void setTestDao(TestDao testDao) {
		this.testDao = testDao;
	}

	public Test findTest(Long id) {
		return testDao.findTest(id);
	}
	
	public int getTestsCount() {
		return testDao.getTestsCount();
	}

	public boolean isNumeric(String input) {
		try {
			Integer.parseInt(input);
			return true;
		} catch (NumberFormatException e) {
			// s is not numeric
			return false;
		}
	}

	public Test answCheck( Long answId) {
		Test tst=null;
		Long nextId = testAttributes.getNextId();

		if (answId != null) {
			if (answId != NO_ID) {
				if (testAttributes.isAnswerCorrect(answId)) {
					Long cnt = testAttributes.getCnt();
					testAttributes.setCnt(++cnt);
				}
			} else {
				testAttributes.setCnt(0l);
			}
		}

		if ((nextId != NO_ID) && (nextId != null)) {
			Long randomTestId = testAttributes.getRandomTestId();
			tst = testDao.findTest(randomTestId);

			Long answerId = Long.valueOf(tst.getAnswerId());

			testAttributes.addAnswerIdToLst(answerId);
		}
		
		return tst;
	}



}
