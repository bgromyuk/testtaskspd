-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.21-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных tester
DROP DATABASE IF EXISTS `tester`;
CREATE DATABASE IF NOT EXISTS `tester` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tester`;


-- Дамп структуры для таблица tester.tests
DROP TABLE IF EXISTS `tests`;
CREATE TABLE IF NOT EXISTS `tests` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Question` varchar(255) NOT NULL DEFAULT '0',
  `AnswerId` int(20) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tester.tests: ~15 rows (приблизительно)
DELETE FROM `tests`;
/*!40000 ALTER TABLE `tests` DISABLE KEYS */;
INSERT INTO `tests` (`Id`, `Question`, `AnswerId`) VALUES
	(1, 'Is ArrayList is synchronized by default or else we can make it as synchronize or not?', 3),
	(2, 'When constructor is called?', 7),
	(3, 'What is boxing?', 10),
	(4, 'How can you force a class to implement only features of our class?', 15),
	(5, 'Among below cases which are true?', 18),
	(6, 'Which wrapper classes have one constructor?', 21),
	(7, 'Which one is better to retrieve the elements in forward and backward direction?', 24),
	(8, 'How to create API doc to a user defined classes?', 27),
	(9, 'What is default priority of thread?', 30),
	(10, 'Which are true among below cases?', 34),
	(11, 'Why @SupressWarning Annotation is used?', 35),
	(12, 'Which method is used for cloning an object?', 39),
	(13, 'What happens if sleep() and wait() executes in synchronized block?', 41),
	(14, 'What is the scope of default access specifier?', 44),
	(15, 'Which alogorithm is used by garabage collector ?', 47);
/*!40000 ALTER TABLE `tests` ENABLE KEYS */;


-- Дамп структуры для таблица tester.variants
DROP TABLE IF EXISTS `variants`;
CREATE TABLE IF NOT EXISTS `variants` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `TestId` int(10) NOT NULL DEFAULT '0',
  `Answer` varchar(200) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_variants_tests` (`TestId`),
  CONSTRAINT `FK_variants_tests` FOREIGN KEY (`TestId`) REFERENCES `tests` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы tester.variants: ~46 rows (приблизительно)
DELETE FROM `variants`;
/*!40000 ALTER TABLE `variants` DISABLE KEYS */;
INSERT INTO `variants` (`Id`, `TestId`, `Answer`) VALUES
	(1, 1, 'ArrayList is synchronized by default'),
	(2, 1, 'ArrayList is not Synchronized we cannot make it synchronized'),
	(3, 1, 'ArrayList is not Synchronized but we can make it using - Collections.synchronizedList(new ArrayList());'),
	(4, 1, 'ArrayList is not Synchronized but we can make it Synchronized using - ArrayList.synchronnizedList(new ArrayLis());'),
	(5, 2, 'Constructor is called before creating the object.'),
	(6, 2, 'Constructor is called after creating the object'),
	(7, 2, 'Constructor is called concurrently when object creation is going on'),
	(9, 2, 'Constructor cannot be called'),
	(10, 3, 'Converting primitive data type to object'),
	(11, 3, 'Converting object data type to Primitive Data Type'),
	(12, 3, 'Creating implemented object to an interface'),
	(13, 3, 'None of the above'),
	(14, 4, 'Only Abstract class'),
	(15, 4, 'Only Interface'),
	(16, 4, 'Abstract class or Interface'),
	(17, 5, 'Abstract and final can be declared to a class'),
	(18, 5, 'Abstract and final are quite contradictory'),
	(19, 6, 'String'),
	(20, 6, 'Integer'),
	(21, 6, 'Character'),
	(22, 7, 'for-each loop'),
	(23, 7, 'Enumeration interface'),
	(24, 7, 'ListIterator Interface'),
	(25, 7, 'Iterator Interface'),
	(26, 8, 'Javac *.java;'),
	(27, 8, 'javadoc *.java;)'),
	(28, 8, 'java *.java'),
	(29, 9, '1'),
	(30, 9, '5'),
	(31, 10, 'String class is immutable, StringBuffer and StringBuilder are mutable'),
	(32, 10, 'StringBuffer Class is synchronized and StringBuilder is not synchronized'),
	(33, 10, 'If programmer want to use several threads then he have to use StringBuffer'),
	(34, 10, 'All the above'),
	(35, 11, 'To suppress exception issued by compiler'),
	(36, 11, 'To show exceptions'),
	(37, 11, 'To show warnings'),
	(38, 12, 'getClass()'),
	(39, 12, 'clone()'),
	(40, 13, 'The object still under lock in both the cases'),
	(41, 13, 'sleep() still under lock ,wait() the lock is removed'),
	(42, 13, 'Sleep() and wait(),for both the method lock is removed'),
	(43, 14, 'default member are available to all the packages'),
	(44, 14, 'default member is available only with in the package'),
	(45, 15, 'Mark'),
	(46, 15, 'Sweep'),
	(47, 15, 'Both a and b');
/*!40000 ALTER TABLE `variants` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
